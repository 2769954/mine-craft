import java.util.Scanner;

/*
 *Result class is used calculate and output the hints about mine configuration 
*/
public class Result {
    public  Result() {
       Field f = new Field();
       if (f.err_flag_getter() == false){
            this.calculate_hints(f);
       }else{
         // do nothing
       }
      
    }

    
   /*
    * This function gets the input values by intantiating class Field and calls
       the functions to calculate the hints for the positions of the mines.
    */
    public void calculate_hints(Field f){
        String[][] s = f.mine_configuration_getter();
        int mine_count = 0;
        int row = f.row_getter();
        int col = f.col_getter();
        String[][] output_array = new String[row][col];
        if( row ==1 && col ==1){ // if only one entry in mine field
            if(s[0][0].equals(".")){
                s[0][0] = Integer.toString(0);
            }else{
                // Do nothing
            }   
        }else if(row !=1 && col ==1){ // if multiple rows but single colunm
            s = hintsOneColunm(s,row,col);
        }else if(row ==1 && col !=1){ // if multiple colunms but single row
            s = hintsOneRow(s,row,col); 
        }else{
            s = hintsNormal(s,row,col);
        }
        show_hints(s,row,col);
    }
    
    
    /*
     * This function takes an "n x m" minefield (where n == 1 and m > 1) and calculates the hints about the position of mines ('*').
     * parameters: s to hold the minefield area in a two dimensional String array
     *             row is total number of rows of minefield
     *             col is total number of rows of minefield
     * Reurn: two dimensional array to hold the hints about the positions of mines
    */
    public String[][]  hintsOneRow(String[][] s,int row,int col){
        int   mine_count = 0;
        for(int q=0;q<col;q=q+1){
                if(s[0][q].equals("*")){
                       // do nothing
                }else{
                    if(q == 0){ // first colunm
                       if(s[0][q+1].equals("*")){
                           mine_count = mine_count+1;
                       }      
                    }else if(q == col-1){ // last column
                       if(s[0][q-1].equals("*")){
                           mine_count = mine_count+1;
                       }      
                    }else{
                        if(s[0][q-1].equals("*")){
                           mine_count = mine_count+1;
                       }if(s[0][q+1].equals("*")){
                           mine_count = mine_count+1;
                       }
                   }
                   s[0][q] = Integer.toString(mine_count);
                   mine_count = 0;
                }   
        } 
        return  s;
    }
    
    
    /*
     * This function takes an "n x m" minefield (where n > 1 and m == 1) and calculates the hints about the position of mines ('*')..
     * parameters: s to hold the minefield area in a two dimensional String array
     *             row is total number of rows of minefield
     *             col is total number of rows of minefield
     * Reurn: two dimensional array to hold the hints about the positions of mines
    */
    public String[][]  hintsOneColunm(String[][] s,int row,int col){
         int   mine_count = 0;
         for(int q=0;q<row;q=q+1){
                if(s[q][0].equals("*")){
                       // do nothing
                }else{
                    if(q == 0){ // first row
                       if(s[q+1][0].equals("*")){
                           mine_count = mine_count+1;
                       }      
                    }else if(q == row-1){ // last row
                       if(s[q-1][0].equals("*")){
                           mine_count = mine_count+1;
                       }      
                    }else{
                        if(s[q-1][0].equals("*")){
                           mine_count = mine_count+1;
                       }if(s[q+1][0].equals("*")){
                           mine_count = mine_count+1;
                       }
                   }
                   s[q][0] = Integer.toString(mine_count);
                   mine_count = 0;
                }   
            }
        return  s;
    }
    
  
     /*
     *   This function takes an "n x m" minefield (where n > 1 and m >1) and calculates the hints about the position of mines ('*').
     * parameters: s to hold the minefield area in a two dimensional String array
     *             row is total number of rows of minefield
     *             col is total number of rows of minefield
     * Reurn: two dimensional array to hold the hints about the positions of mines
    */
    public String[][]  hintsNormal(String[][] s,int row,int col){
        int   mine_count = 0;
        for(int i = 0; i<row; i=i+1){
                for(int j = 0; j<col; j=j+1){
                    if(s[i][j].equals("*")){
                       // do nothing
                    }else{
                      if (i == 0){ // line 0
                            if(j==0){ // first entry of mine field
                                if(s[i][j+1].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i+1][j].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i+1][j+1].equals("*")){
                                    mine_count = mine_count+1;
                                }
                            }else if (j == col-1){ // last entry of mine field's first row
                                 if(s[i][j-1].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i+1][j].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i+1][j-1].equals("*")){
                                    mine_count = mine_count+1;
                                }
                            }else{  // middle enteries of first row
                                if(s[i][j+1].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i][j-1].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i+1][j].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i+1][j+1].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i+1][j-1].equals("*")){
                                    mine_count = mine_count+1;
                                }
                            }
                            s[i][j] = Integer.toString(mine_count);
                        }else if(i == row-1){ // last line
                            if(j==0){ // first entry of last row
                                if(s[i][j+1].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i-1][j].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i-1][j+1].equals("*")){
                                    mine_count = mine_count+1;
                                }
                             
                            }else if(j== col-1){ // last entry of last row
                                if(s[i][j-1].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i-1][j].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i-1][j-1].equals("*")){
                                    mine_count = mine_count+1;
                                }
                            
                            }else{ // middle enteries of last row
                                if(s[i][j-1].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i][j+1].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i-1][j].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i-1][j-1].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i-1][j+1].equals("*")){
                                    mine_count = mine_count+1;
                                }       
                            }
                            s[i][j] = Integer.toString(mine_count);
                        }else{  // Middle lines
                            if(j==0){ // first entry of middle(not first or last row) rows
                                if(s[i][j+1].equals("*")){
                                    mine_count = mine_count+1;
                                } if(s[i-1][j].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i-1][j+1].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i+1][j].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i+1][j+1].equals("*")){
                                    mine_count = mine_count+1;
                                }
                            }else if(j == col-1){ // Last entry of middle(not first or last row) rows
                                if(s[i][j-1].equals("*")){
                                    mine_count = mine_count+1;
                                } if(s[i-1][j].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i-1][j-1].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i+1][j].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i+1][j-1].equals("*")){
                                    mine_count = mine_count+1;
                                }
                            }else{
                                if(s[i][j-1].equals("*")){
                                    mine_count = mine_count+1;
                                } if(s[i][j+1].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i-1][j-1].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i-1][j].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i-1][j+1].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i+1][j+1].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i+1][j].equals("*")){
                                    mine_count = mine_count+1;
                                }if(s[i+1][j-1].equals("*")){
                                    mine_count = mine_count+1;
                                }
                            }
                            s[i][j] = Integer.toString(mine_count);
                        }
                   
                    }
                    mine_count = 0;
                }
                   
            }
         return  s; 
    }
    
  
     /*
     * This function prints out the output.
     * parameters: array to hold the minefield area in a two dimensional String array
     *             row is total number of rows of minefield
     *             col is total number of rows of minefield
    */
    public void show_hints(String[][] array,int row,int col){
         System.out.println(" ");
         for(int q=0;q<row;q=q+1){
            for(int p=0;p<col;p=p+1){
                System.out.print(array[q][p]);
            }
            System.out.println(" ");
        }
    }
}