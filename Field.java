import java.util.Scanner;

/*
 *Field class is used to take and validate all the inputs(size of mine field, configuration of mines) from the user
*/
public class Field {
       
        //to hold input configuration of minefield
        private String[][] mine_configuration;
        
        //total rows in minefield
        private int rows;
        
        //total colunms in minefield
        private int colunms;
        
        //it will be true if the validation of any input value fails
        private boolean err_flag = false;
        
        public Field(){
            field_size();
        }
        
        
        // This function takes the input size of the mine field (i.e, 3x5) from the user
        public void field_size(){
            System.out.println("Start of New Game!");
            Scanner scanner = new Scanner(System.in);
            System.out.println("Please Enter the Size of the board");
            String size = scanner.nextLine();
            String[] splited_size = size.split(" ");
            if (splited_size.length == 2){
                try{
                    rows = Integer.parseInt(splited_size[0]);
                    colunms = Integer.parseInt(splited_size[1]);
                    mine_configuration(rows,colunms);
                 }catch(NumberFormatException ex){ // handle your exception
                    System.out.println("Error: Only an integer value is allowed");
                    err_flag = true;
                }
              
            }else{
                System.out.println("Wrong size!, Enter two digits with single space in between");
                err_flag = true;
            }
          
        }
        
        
        /*
         * This function takes the size of the mine field(rows x colunm) and populate
            all the entries of the mine field with "." or "*" characters
         * parameters: row is total number of rows of minefield
                       col is total number of rows of minefield
        */
         public void mine_configuration(int row, int col){
             String configuration = "";
             mine_configuration = new String[row][col];
             System.out.println("Enter the configuration of the mines");
             Scanner scanner = new Scanner(System.in);
             for(int i=0; i<row; i=i+1){
                configuration = scanner.nextLine();
                 //This is indeed a regex-expression, called a negative lookahead
                 //"cat".toCharArray()
                String[] split_configuration = configuration.split("(?!^)");
                if (split_configuration.length == col){
                   if(character_validation(split_configuration) == true){
                        for(int j=0; j< col; j=j+1){
                           mine_configuration[i][j] = split_configuration[j];        
                        }   
                    }else{
                        System.out.println("Error: Only '*' or '.' characters are allowed");
                        i=row;
                        err_flag = true;
                    }
                }else{
                    System.out.println("Error: Enter "+ col+" characters in each row");
                    err_flag = true;
                    i=row;
                }
            }
    
        }
         
        
           
        /*
         * This function takes an array and checks if each character is either "*" or "."
         * parameters: split_configuration to hold the input configuration
         *             
         * Reurn: true if only "*" or "." characters are present.
        */
        public boolean character_validation(String[] split_configuration){
                boolean cFlag = true ;
                for(int j=0; j< split_configuration.length; j=j+1){ 
                    if(split_configuration[j].equals(".") ||  split_configuration[j].equals("*")){
                         //do Nothing   
                    }else{
                        cFlag = false ;
                    }
                }
                return cFlag ;
        }
        
        
        // This function returns the mine field configuration 
        public String[][] mine_configuration_getter(){
            return this.mine_configuration;
        }
        
        
        // This function returns the mine field number of rows
        public int row_getter(){
            return this.rows;
        }
        
        // This function returns the mine field number of colunms
        public int col_getter(){
            return this.colunms;
        }
        
         // This function returns the err_flag which indicates if all the input values are correct or not
        public boolean err_flag_getter(){
            return this.err_flag;
        }
    }
