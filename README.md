# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
Minesweeper is a single-player puzzle video game played on an n by m field of squares. The objective of the game is to clear a field containing hidden �mines� without detonating any of them, with help from clues about the number of neighbouring mines in each square.
This is a command-line based, object-oriented application that will first accept the size of a field followed by the configuration of the mines in that field. The application will then print out the field with the blank (unmined) squares filled in with the appropriate hints

### How do I get set up? ###

Download java. Run MineSweeper.java through command line.
